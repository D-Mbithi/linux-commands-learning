# Linux Commands Learning

This is online journal of new linux commands I learned or new options for commands I already know.
With Linuxm, there is always another way to skin the 'cat'.

## 1. ls

This is a command used to list content of directory in CLI. There are numberous options you can use like the following:

- ```-a``` list all files and directory in the directory including the hiden ones.

```bash
ls -a <directory>/
```

- ```-l``` stand for long info, it provide more detailed information about the listing, files and directories.

```bash
ls -l <directory>/
```
